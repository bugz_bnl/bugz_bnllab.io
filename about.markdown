---
layout: page
title: About
permalink: /about/
---
## ---
# Whoami

<a href="/assets/img/hendrik.jpg" data-lity>
  <img src="/assets/img/hendrik.jpg" height="300px"/>
</a>

[![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/noben-hendrik/) [![Twitter](https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/bugz_genk) [![GitHub](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/He-No) 

First of all, I am a cybersecurity enthusiast, with a focus on penetration testing (employed by the Security Factory), with hands-on experience in Infrastructure, WebApp, Binary and WiFi environments. Besides this great job, I am active as an esports manager focusing on the CS:GO division of KRC Genk.

I am very grateful and happy that I can spend most of my time practicing my two hobbies on a daily basis. There is no such thing as a free lunch, to get what you want you have to work hard. But I can say first hand that hard work pays off!

## ---

Work Experience (IT & Cybersecurity)
------------------------------------

Feb 2021 - Present
:   **[the Security Factory](http://thesecurityfactory.be/), IT Security Consultant**

    - Infrastructure Penetration tester 
    - (Web)App Penetration tester
    - Vulnerabiliy manager for [Digipolis](https://www.digipolisantwerpen.be/)

Sep 2020 - Feb 2021
:   **[NTT Ltd.](https://global.ntt/), IT Security Assessment Consultant**

    - Security Researcher 
    - (Web)Application Penetration tester
    - Red Teamer

Feb 2020 - May 2020
:   **[the Security Factory](https://thesecurityfactory.be/), Intern**

    - Thesis: Prevention measures for stack-based buffer overflows on Unix systems
    - Internship Assignment: Set-up of a real-life applicable vulnerable network

Jul 2018 - Jul 2019
:   **[Open Line BV.](https://openline.nl/), Service Desk, Student**

    - I combined my studies at PXL with a part-time job at OpenLine. That way I gained some experience in the field and came in touch with various cutting-edge technologies.


Work Experience (Esport)
------------------------------------

Sep 2021 - Present
:   **[KRC Genk](https://krcgenk.be), Esport Manager**

    - Startup and management of Counter-Strike: Global Offensive (CSGO) division

Dec 2020 - Sep 2021
:   **[Join The Force](https://www.jointheforce.net/), Operational Organization Manager**

    - Startup and management of all esport divisions (Counter-Strike: Global Offensive, Rocket League, Rainbow 6, League of Legends)
    - Brand Responsible 
    - Event Management
    - Partnership Management


Feb 2021 - Present
:   **[Low Land Lions (Defusekids)](https://lowlandlions.com/), Head Coach**

    - Improving the cohesion
    - Improving the communication 
    - Demo analysis
    - Handling communication between players and staff
    - Setup and management of game- and voice servers


Certifications and Courses
---------------------------

Apr 2020
:   **TCM Security**, [Practical Ethical Hacking - The Complete Course](https://www.udemy.com/certificate/UC-fcb369bb-a1ea-40c8-a339-66bfc955686a/)**


Education
---------

Sep 2016 - Jun 2020
:   **University College PXL, Bachelor's degree, Magna Cum Laude**

    - Applied Information Technology Science: Systems and Networks


Projects
-----------------

[ntlmrelayx2proxychains](https://github.com/He-No/ntlmrelayx2proxychains), Python3 tool
:   More hereover in my [blogpost](/_posts/2022-05-12-ntlmrelayx2proxychains.markdown)
