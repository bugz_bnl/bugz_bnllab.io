---
layout: post #Do not change.
category: [Esport] #One, more categories or no at all.
title: "Renewed CSGO Team" #Article title.
author: BugZ #Author's nick.
#nextPart: _posts/2021-01-30-example.md #Next part.
#prevPart: _posts/2021-01-30-example.md #Previous part.
---


**KRC Genk’s esports division is, next to FIFA and League of Legends, also active in Counter-Strike: Global Offensive (CS:GO). Soon the CS:GO team will start for the second consecutive season in the Elite Series: Counter-Strike (ESCS), one of the most important Benelux competitions. Last season, KRC Genk Esports managed to achieve a great third place. The goal this season: to do better than that third place. To achieve that, KRC Genk Esports has unveiled a new CS:GO team with three new players and one new shirt sponsor: GGPoker.**

<div markdown=1 class="sx-center">
<a href="/assets/img/Genk_S40.jpg" data-lity>
  <img src="/assets/img/Genk_S40.jpg" height="300px"/>
</a>
</div>

After weeks of thorough scouting, the result is a squad composed of a mix of experienced and talented Benelux players. Kik “Fishie” Voortman and Rune “Tewyy” Steusel, two Dutchmen, signed for an extended stay and are surrounded by three up-and-coming talents. Giovanni “Wumbo” Türkeli, also a Dutchman, will make life difficult for opponents with the AWP (sniper). Two very young Belgian players will also join the team: Kyan “AyeZs” Blockx, known for his great communication skills and smart decision making, and Roman “n0te” Hamze, known for his sharp aim and merciless entry fragging.

Lineup:
- 🇳🇱 Kik “Fishie” Voortman
- 🇳🇱 Rune “Tewy” Steusel
- 🇳🇱 Giovanni “Wumbo” Türkeli
- 🇧🇪 Kyan “AyeZs” Blockx
- 🇧🇪 Roman “n0te” Hamze

Staff:
- 🇧🇪 Hendrik “BugZ” Noben (Manager)

“We are entering into a long-term commitment with the players. By doing so, we are expressing confidence, stability and ambition. With this renewed team we will put KRC Genk Esports on the national and international radar within the CS:GO scene”, said Hendrik Noben, Esport Manager CS:GO at KRC Genk Esports.